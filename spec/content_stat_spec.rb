require './spec/spec_helper'
require 'pstore'

describe ContentStat do
  before do
    FileUtils.rm ContentStat.table_name, force: true
  end

  after do
    FileUtils.rm ContentStat.table_name, force: true
  end

  describe "#initialize" do
    it "initialize attributes" do
      content_stat = ContentStat.new("abcd")
      content_stat.content.must_equal "abcd"
      content_stat.count.must_equal 0
    end
  end

  describe ".all" do
    before do
      ContentStat.migrate
      storage = PStore.new ContentStat.table_name
      storage.transaction do
        abc = ContentStat.new('abc')
        abcd = ContentStat.new('abcd')
        abc.instance_variable_set(:@count, 2)
        abcd.instance_variable_set(:@count, 3)

        storage["abc"] = abc
        storage["abcd"] = abcd
      end
    end

    it "returns array of ContentStat" do
      result = ContentStat.all

      result.class.must_equal(Array)
      result[0].content.must_equal("abc")
      result[0].count.must_equal(2)
      result[1].content.must_equal("abcd")
      result[1].count.must_equal(3)
    end
  end

  describe ".migrate" do
    it "generate file based storage called content_stat.pstore" do
      assert_equal false, File.exist?(ContentStat.table_name)
      ContentStat.migrate
      assert File.exist?(ContentStat.table_name)
    end
  end

  describe "#find_count_by_content" do
    before do
      ContentStat.migrate
      storage = PStore.new ContentStat.table_name
      storage.transaction do
        abc = ContentStat.new('abc')
        abcd = ContentStat.new('abcd')
        abc.instance_variable_set(:@count, 2)
        abcd.instance_variable_set(:@count, 3)

        storage["abc"] = abc
        storage["abcd"] = abcd
      end
    end

    it "returns stored content_stat's count match by given content" do
      count = ContentStat.find_count_by_content 'abc'
      count.must_equal(2)

      count = ContentStat.find_count_by_content 'abcd'
      count.must_equal(3)
    end
    it "returns 0 when given content not found in storage" do
      count = ContentStat.find_count_by_content 'abcde'
      count.must_equal(0)
    end
  end

  describe "#save" do
    it "store content_stat to storage with count 1 if content_stat by given content doesn't exist" do
      content_scan = ContentStat.new('abcd')
      content_scan.save

      expected_count = ContentStat.find_count_by_content('abcd')
      expected_count.must_equal(1)
    end

    it "accumulate stored existing content_stat's count if content_stat by given content exist" do
      content_scan = ContentStat.new('abcde')
      content_scan.save

      other_content_scan = ContentStat.new('abcde')
      other_content_scan.save

      expected_count = ContentStat.find_count_by_content('abcde')
      expected_count.must_equal(2)
    end
  end
end

