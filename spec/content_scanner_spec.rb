require './spec/spec_helper'
require 'pstore'

describe ContentScanner do
  describe "#run" do
    it "return content which has the highest number of occurrences." do
      given_path = File.join(TEST_ROOT, 'fixtures', 'example')
      result = ContentScanner.new.run(given_path)

      result.content.must_equal("abcdef")
      result.count.must_equal(4)

      FileUtils.rm(ContentStat.table_name)
    end

    it "return nil when path invalid" do
      given_path = File.join(TEST_ROOT, 'fixtures', 'notfound')
      result = ContentScanner.new.run(given_path)

      result.must_be_nil
    end
  end

  describe "#execute" do
    after do
      FileUtils.rm(ContentStat.table_name)
    end

    it "read every single file, store file content number of occurences" do
      given_path = File.join(TEST_ROOT, 'fixtures', 'example')
      ContentScanner.new.execute(given_path)

      storage = PStore.new(ContentStat.table_name)

      storage.transaction(true) do
        storage.roots.size.must_equal(2)

        storage['abcdef'].content.must_equal('abcdef')
        storage['abcdef'].count.must_equal(4)

        storage['abcdefghijkl'].content.must_equal('abcdefghijkl')
        storage['abcdefghijkl'].count.must_equal(1)
      end
    end
  end

  describe "#ls" do
    it "returns list of all items inside when given path is directory" do
      given_path = File.join TEST_ROOT, 'fixtures', 'example'
      expected_items =["/content_scan/spec/fixtures/example/A",
                       "/content_scan/spec/fixtures/example/B",
                       "/content_scan/spec/fixtures/example/content1"] 

      result = ContentScanner.new.ls given_path
      result.must_equal(expected_items)
    end

    it "returns empty because not directory" do
      given_path = File.join TEST_ROOT, 'fixtures', 'example', 'B', 'content1'

      result = ContentScanner.new.ls given_path
      result.must_equal([])
    end
  end

  describe "#store" do
    before do
      @path1 = File.join TEST_ROOT, 'fixtures', 'temp1'
      @path2 = File.join TEST_ROOT, 'fixtures', 'temp2'
    end

    after do
      FileUtils.rm(ContentStat.table_name)
    end

    it "read file content and save to storage" do
      file_content = File.read(@path1)
      content_scanner = ContentScanner.new
      content_scanner.store @path1
      content_scanner.store @path2

      storage = PStore.new(ContentStat.table_name)

      storage.transaction(true) do
        storage.roots.size.must_equal(1)
        storage[file_content].content.must_equal(file_content)
        storage[file_content].count.must_equal(2)
      end
    end
  end
end
