require 'pstore'

class ContentStat
  attr_accessor :content, :count

  def initialize(content)
    self.content = content
    self.count = 0
  end

  def self.table_name
    "content_stat.pstore"
  end

  def self.migrate
    storage.transaction {}
  end

  def self.all
    result = Array.new
    storage.transaction(true) do
      storage.roots.each do |key|
        result << storage[key]
      end
    end
    result
  end

  def self.find_count_by_content(content)
    content_stat = storage.transaction(true) do
      storage[content]
    end
    content_stat&.count.to_i
  end

  def save
    storage = ContentStat.storage
    storage.transaction(false) do
      existing = storage[content] || self
      existing.count += 1
      storage[content] = existing
    end
  end

  private
  def self.storage
    @@storage ||= PStore.new(table_name)
  end
end
