class ContentScanner
  def run(path)
    execute(path)
    ContentStat.all.sort do |x,y|
      y.count <=> x.count
    end.first
  end

  def execute(path)
    ls(path).each do |el|
      File.file?(el) ? store(el) : execute(el)
    end
  end

  def ls(path)
    scanned_path = File.join(path, "*")
    Dir[scanned_path].sort
  end

  def store(path)
    File.readlines(path).each do |line|
      content_stat = ContentStat.new(line)
      content_stat.save
    end
  end
end
